FROM golang:1.12-alpine AS build_base

RUN apk add --no-cache git
RUN apk add build-base

# Set the Current Working Directory inside the container
WORKDIR /tmp/discord-social-media-messenger

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Build the Go app
RUN go build -o ./bot .

FROM alpine:3.9 
RUN apk add ca-certificates

COPY --from=build_base /tmp/discord-social-media-messenger/bot /bot
COPY --from=build_base /tmp/discord-social-media-messenger/configs/ /configs/

# Run the binary program produced by `go install`
CMD ["/bot"]