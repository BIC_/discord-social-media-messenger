# Discord Social Media Messenger

Takes images/text from Discord channels and posts it to social media services.
Supported Services:
- Twitter
- Facebook (in-dev)

## Docker

### Docker Build
```bash
docker build -t discord-social-media-messenger .
```

### Docker Run With Migration
```bash
docker run \
-e ENVIRONMENT=docker \
-e DISCORD_TOKEN= \
-e TWITTER_CONSUMER_API_KEY= \
-e TWITTER_CONSUMER_API_KEY_SECRET= \
-e TWITTER_ACCESS_TOKEN= \
-e TWITTER_ACCESS_TOKEN_SECRET= \
-e POSTGRES_USERNAME= \
-e POSTGRES_PASSWORD= \
-e MIGRATE=TRUE \
discord-social-media-messenger
```