package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/ChimeraCoder/anaconda"
	"github.com/bwmarrin/discordgo"
	fb "github.com/huandu/facebook"
	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_/discord-social-media-messenger/models"
	"gitlab.com/BIC_/discord-social-media-messenger/services"
	"gitlab.com/BIC_/discord-social-media-messenger/utils"
	"gitlab.com/BIC_/discord-social-media-messenger/utils/db"
)

// Post struct
type Post struct {
	DiscordMessageID string
	MessageAuthorID  string
	MessageAuthor    string
	Description      string
	MediaURLs        []string
}

// Config data for the bot
var Config *utils.Config

func main() {
	env := os.Getenv("ENVIRONMENT")
	discordToken := os.Getenv("DISCORD_TOKEN")

	Config = utils.GetConfig(env)

	if os.Getenv("MIGRATE") == "TRUE" {
		migrateTables()
	}

	dg, discErr := discordgo.New("Bot " + discordToken)

	if discErr != nil {
		fmt.Println("error creating Discord session,", discErr)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// Register the messageDelete func as a callback for MessageDelete events
	dg.AddHandler(messageDelete)

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		fmt.Println("error opening connection,", openErr)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !isWhitelistedChannel(m.ChannelID) {
		return
	}

	if m.Author.ID == s.State.User.ID {
		return
	}

	if len(m.Attachments) <= 0 {
		return
	}

	DBStruct := getDB()
	defer DBStruct.GetDB().Close()

	id := m.ID
	message := m.Content
	authorID := m.Author.ID
	author := m.Author.Username

	var imageUrls []string
	maxImageSizeBytes := 5000000

	for _, v := range m.Attachments {
		if v.Size > maxImageSizeBytes {
			continue
		}

		if !validImageType(v.Filename) {
			continue
		}

		imageUrls = append(imageUrls, v.URL)
	}

	if len(imageUrls) <= 0 {
		return
	}

	post := Post{
		DiscordMessageID: id,
		MessageAuthorID:  authorID,
		MessageAuthor:    author,
		Description:      message,
		MediaURLs:        imageUrls,
	}

	_, twitterPostErrors := createTwitterPost(&post, &DBStruct)

	if len(twitterPostErrors) > 0 {
		for _, err := range twitterPostErrors {
			fmt.Printf("Failed to create twitter post with error: %s\n", err.Error())
		}
	}

	guild, guildErr := s.Guild(m.GuildID)

	if guildErr != nil {
		fmt.Println(guildErr)
	}

	twitterEmojiID := Config.Twitter.EmojiDefault

	for _, emoji := range guild.Emojis {
		if emoji.Name == Config.Twitter.EmojiCustom {
			twitterEmojiID = emoji.APIName()
		}
	}

	reactionError := s.MessageReactionAdd(m.ChannelID, m.Message.ID, twitterEmojiID)

	if reactionError != nil {
		fmt.Println(reactionError.Error())
	}

	// Write the image and message to Facebook
	// Write Discord message ID and Facebook Post ID to DB
	/*
		_, facebookPostErrors := createFacebookPost(&post, DB)

		if len(facebookPostErrors) > 0 {
			for _, err := range facebookPostErrors {
				fmt.Printf("Failed to create facebook post with error: %s\n", err.Error())
			}
		}
	*/
}

func messageDelete(s *discordgo.Session, m *discordgo.MessageDelete) {
	if !isWhitelistedChannel(m.ChannelID) {
		return
	}

	DBStruct := getDB()
	defer DBStruct.GetDB().Close()

	post := Post{
		DiscordMessageID: m.ID,
	}

	deleteTwitterErrors := deleteTwitterPost(&post, &DBStruct)

	if deleteTwitterErrors != nil {
		for _, err := range deleteTwitterErrors {
			fmt.Printf("Failed to delete twitter post with error: %s\n", err.Error())
		}
	}

	// Look up Discord message ID in DB from Facebook table
	// Delete image from Facebook by Facebook Post ID
}

func getTwitterClient() *anaconda.TwitterApi {
	twitterConsumerKey := os.Getenv("TWITTER_CONSUMER_API_KEY")
	twitterConsumerKeySecret := os.Getenv("TWITTER_CONSUMER_API_KEY_SECRET")
	twitterAccessToken := os.Getenv("TWITTER_ACCESS_TOKEN")
	twitterAccessTokenSecret := os.Getenv("TWITTER_ACCESS_TOKEN_SECRET")

	return anaconda.NewTwitterApiWithCredentials(twitterAccessToken, twitterAccessTokenSecret, twitterConsumerKey, twitterConsumerKeySecret)
}

func getDB() db.Interface {
	var DBStruct db.Interface

	switch Config.DB.Type {
	case "SQLite3":
		SQLite3 := db.SQLite3{}
		SQLite3.Connect(Config)
		DBStruct = &SQLite3
	case "PostgreSQL":
		PostgreSQL := db.PostgreSQL{
			Username: os.Getenv("POSTGRES_USERNAME"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
		}
		PostgreSQL.Connect(Config)
		DBStruct = &PostgreSQL
	default:
		log.Fatal("Invalid DB type")
	}

	return DBStruct
}

func createTwitterPost(p *Post, DBStruct *db.Interface) ([]*anaconda.Tweet, []error) {
	twitterAPI := getTwitterClient()
	twitter := services.Twitter{
		Client: twitterAPI,
	}

	var tweets []*anaconda.Tweet
	var errors []error

	for _, url := range p.MediaURLs {
		twitterPost := services.TwitterPost{
			MediaURL:         url,
			MediaDescription: p.Description,
			MediaAuthor:      p.MessageAuthor,
		}

		aTweet, err := twitter.CreatePost(&twitterPost)

		if err != nil {
			errors = append(errors, err)
			continue
		}

		tweets = append(tweets, aTweet)

		twitterDB := models.Twitter{
			DiscordID:        p.DiscordMessageID,
			AuthorID:         p.MessageAuthorID,
			AuthorName:       p.MessageAuthor,
			TwitterID:        aTweet.IdStr,
			MediaURL:         twitterPost.MediaURL,
			MediaDescription: p.Description,
		}

		twitterDBErr := twitterDB.Create(*DBStruct)

		if twitterDBErr != nil {
			errors = append(errors, twitterDBErr)
		}
	}

	return tweets, errors
}

func deleteTwitterPost(p *Post, DBStruct *db.Interface) []error {
	twitterDB := models.Twitter{
		DiscordID: p.DiscordMessageID,
	}

	tweets, twitterDBGetErr := twitterDB.GetAll(*DBStruct)

	var deleteErrors []error

	if twitterDBGetErr != nil {
		fmt.Printf("Delete Error: %s\n", twitterDBGetErr)
		deleteErrors = append(deleteErrors, twitterDBGetErr)
		return deleteErrors
	}

	twitterAPI := getTwitterClient()
	twitter := services.Twitter{
		Client: twitterAPI,
	}

	for _, aTweet := range tweets {
		twitterID, convErr := strconv.Atoi(aTweet.TwitterID)

		if convErr != nil {
			deleteErrors = append(deleteErrors, convErr)
		}

		_, deleteErr := twitter.DeletePost(int64(twitterID))

		if deleteErr != nil {
			deleteErrors = append(deleteErrors, deleteErr)
			continue
		}

		deleteDBErr := aTweet.Delete(*DBStruct)

		if deleteDBErr != nil {
			deleteErrors = append(deleteErrors, deleteDBErr)
			continue
		}
	}

	return nil
}

func createFacebookPost(p *Post, DB *gorm.DB) ([]*fb.Result, []error) {
	facebook := services.Facebook{
		AuthToken: os.Getenv("FACEBOOK_APP_ACCESS_TOKEN"),
		PageID:    os.Getenv("FACEBOOK_PAGE_ID"),
	}

	var posts []*fb.Result
	var errors []error

	for _, url := range p.MediaURLs {
		facebookPost := services.FacebookPost{
			MediaURL:         url,
			MediaDescription: p.Description,
			MediaAuthor:      p.MessageAuthor,
		}

		fbPost, createPostErr := facebook.CreatePost(&facebookPost)

		if createPostErr != nil {
			fmt.Println(createPostErr.Error())
			errors = append(errors, createPostErr)
			continue
		}

		posts = append(posts, fbPost)
	}

	return posts, errors
}

func addReaction(m *discordgo.MessageCreate, reactionType string) {

}

// validImageType checks if the image sent in discord is a valid format for Twitter
func validImageType(imageName string) bool {
	validTypes := []string{
		".jpg",
		".jpeg",
		".png",
	}

	for _, v := range validTypes {
		if strings.Contains(imageName, v) {
			return true
		}
	}

	return false
}

func isWhitelistedChannel(channelID string) bool {
	for _, ID := range Config.Discord.ChannelWhitelist {
		if ID == channelID {
			return true
		}
	}

	return false
}

func migrateTables() {
	fmt.Println("Migrating tables: twitter, facebook")

	DBStruct := getDB()
	defer DBStruct.GetDB().Close()

	DBStruct.Migrate(models.Twitter{})
	DBStruct.Migrate(models.Facebook{})
}
