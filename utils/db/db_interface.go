package db

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_/discord-social-media-messenger/utils"
)

// Interface DB interface
type Interface interface {
	Connect(c *utils.Config) *utils.ModelError
	Migrate(table interface{}) *utils.ModelError
	GetDB() *gorm.DB
}
