package services

import (
	"fmt"

	fb "github.com/huandu/facebook"
)

// FacebookPost struct
type FacebookPost struct {
	MediaURL         string
	MediaDescription string
	MediaAuthor      string
}

// Facebook struct
type Facebook struct {
	AuthToken string
	PageID    string
}

// CreatePost creates a post in Facebook
func (f Facebook) CreatePost(fbp *FacebookPost) (*fb.Result, error) {
	path := fmt.Sprintf("/%s/feed", f.PageID)
	params := fb.Params{
		"access_token": f.AuthToken,
		"message":      "test message",
	}

	result, err := fb.Post(path, params)

	if err != nil {
		return nil, err
	}

	return &result, nil
}
