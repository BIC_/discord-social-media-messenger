package services

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/ChimeraCoder/anaconda"
)

// TwitterPost struct
type TwitterPost struct {
	MediaURL         string
	MediaDescription string
	MediaAuthor      string
}

// Twitter struct
type Twitter struct {
	Client *anaconda.TwitterApi
}

// MaxTweetLength value
var MaxTweetLength = 280

// TwitterNewline string
var TwitterNewline = "\n"

// TwitterByline string
var TwitterByline = "By: "

// CreatePost creates a twitter post
func (t *Twitter) CreatePost(tp *TwitterPost) (*anaconda.Tweet, error) {
	imageBase64, err := getBase64ImageByURL(tp.MediaURL)

	if err != nil {
		return nil, err
	}

	media, err := t.uploadMedia(imageBase64)

	if err != nil {
		return nil, err
	}

	tweetMessage := "Aftermath Image Gallery"

	if tp.MediaDescription != "" {
		tweetMessage = tp.MediaDescription
	}

	formattedMessage := formatMessage(tp.MediaAuthor, tweetMessage)

	values := url.Values{
		"media_ids": {media.MediaIDString},
	}

	tweet, err := t.Client.PostTweet(formattedMessage, values)

	if err != nil {
		return nil, err
	}

	return &tweet, nil
}

// DeletePost deletes a twitter post
func (t *Twitter) DeletePost(id int64) (*anaconda.Tweet, error) {
	tweet, err := t.Client.DeleteTweet(id, false)

	if err != nil {
		return nil, err
	}

	return &tweet, nil
}

func getBase64ImageByURL(url string) (string, error) {
	var client http.Client
	resp, err := client.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("Failure response when getting image URL: %d", resp.StatusCode)
	}

	imageBytes, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "", err
	}

	return base64.RawStdEncoding.EncodeToString(imageBytes), nil
}

func (t *Twitter) uploadMedia(mediaBase64 string) (*anaconda.Media, error) {
	media, err := t.Client.UploadMedia(mediaBase64)

	if err != nil {
		return nil, err
	}

	return &media, nil
}

func formatMessage(author string, description string) string {
	authorLength := len(author)
	descriptionLength := len(description)
	twitterNewlineLength := len(TwitterNewline)
	twitterBylineLength := len(TwitterByline)

	if authorLength+descriptionLength+twitterNewlineLength+twitterBylineLength <= MaxTweetLength {
		return fmt.Sprintf("%s%s%s%s", description, TwitterNewline, TwitterByline, author)
	}

	maxDescriptionLength := MaxTweetLength - twitterNewlineLength - twitterBylineLength - authorLength - 3

	truncatedDescription := fmt.Sprintf("%s%s", description[0:maxDescriptionLength], "...")

	return fmt.Sprintf("%s%s%s%s", truncatedDescription, TwitterNewline, TwitterByline, author)
}
